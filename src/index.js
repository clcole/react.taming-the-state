import React from 'react';
import ReactDOM from 'react-dom';
import { combineReducers, createStore } from 'redux';
import {Provider, connect} from 'react-redux';
import './index.css';
// import App from './App';

/*
  view -> action -> reducer -> store -> view

  v - the view dispatches an action
  a - an action describes what happened
  r - the reducer receives the current state and action, and returns a new state
  s - the store saves the state and notifies any subscribers to update the view

  mapStateToProps -> view -> mapDispatchToProps
  
  mapStateToProps() - retrieve state in the view layer 
  mapDispatchToProps() - alter state in the view layer
*/

//action types
const TODO_ADD = "TODO_ADD";
const TODO_TOGGLE = "TODO_TOGGLE";
const FILTER_SET = "FILTER_SET";

//more constants
const visibilityFilters = {
  SHOW_ALL: "SHOW_ALL",
  SHOW_ACTIVE: "SHOW_ACTIVE",
  SHOW_COMPLETED: "SHOW_COMPLETED"
};

const { SHOW_ALL, SHOW_COMPLETED } = visibilityFilters;

//action creators
function addTodo(id, name) {
  return {
    type: TODO_ADD,
    payload: { id, name }
  };
}

function toggleTodo(id) {
  return {
    type: TODO_TOGGLE,
    payload: id
  };
}

function setFilter(filter) {
  return {
    type: FILTER_SET,
    payload: filter
  };
}

//slice reducer receives its slice of state
function todoReducer(state = [], action) {
  // console.log("in todoReducer ", state);
  switch (action.type) {
    case TODO_ADD: {
      return [
        ...state,
        {
          ...action.payload,
          completed: false
        }
      ];
    }
    case TODO_TOGGLE: {
      return state.map(todo => {
        return todo.id === action.payload
          ? { ...todo, completed: !todo.completed }
          : todo;
      });
    }
    default: return state;
  }
}

//slice reducer receives its slice of state
function filterReducer(state = SHOW_ALL, action) {
  // console.log("in filterReducer ", state);
  switch (action.type) {
    case FILTER_SET:
      return action.payload;
    default:
      return state;
  }
}

//combineReducers calls all slice reducers it wraps
const rootReducer = combineReducers({
  todos: todoReducer,
  filter: filterReducer
});
    //equivalent to
    // function rootReducer(state = {}, action) {
    //   // console.log("in rootReducer");
    //   return {
    //     todos: todoReducer(state.todos, action),
    //     filter: filterReducer(state.filter, action)
    //   };
    // }

//before the first action is dispatched, the store will 
//initialize by calling the reducer
const store = createStore(rootReducer);

const unsubscribe = store.subscribe(() => console.log(store.getState()));

store.dispatch(addTodo(0, "Learn React"));
store.dispatch(addTodo(1, "Learn Redux"));
store.dispatch(toggleTodo(0));
store.dispatch(addTodo(2, "Learn RWD"));
store.dispatch(setFilter(SHOW_COMPLETED));

unsubscribe();

function TodoItem({todo, onToggleTodo}) {
  const {id, name, completed} = todo;
  return (
    <div>
      {name}
      <button 
        onClick={() => onToggleTodo(id)}
      >
        {completed ? "Complete" : "Incomplete"}
      </button>
    </div>
  );
}

function TodoList({todos}) {
  return (
    <div>
      {todos.map(todo => (<ConnectedTodoItem
        key={todo.id}
        todo={todo}
      />))}
    </div>
  );
}

function TodoApp() {
  return (
    <ConnectedTodoList />
  );
}

//the returned object will be merged into the props of the connected component
function mapStateToProps(state) {
  return {
    todos: state.todos
  };
}

//the returned object will be merged into the props of the connected component
function mapDispatchToProps(dispatch) {
  return {
    onToggleTodo: id => dispatch(toggleTodo(id))
  };
}

/*
connect is a HOC that returns an enhanced component
the input component will receive access to the global state
and/or the dispatch method of the store
*/
// const ConnectedTodoApp = connect(mapStateToProps, mapDispatchToProps)(TodoApp);
const ConnectedTodoList = connect(mapStateToProps)(TodoList);
const ConnectedTodoItem = connect(null, mapDispatchToProps)(TodoItem);

function render() {
  ReactDOM.render(
    //This gives every child component in the tree access to the store
    <Provider store={store}>
      <TodoApp />
    </Provider>,
    document.getElementById('root'));
}

store.subscribe(render);
render();
